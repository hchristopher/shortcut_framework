/*
functions that can be called throughout the modules belong here
*/

function getColSize(col) {

    var classname = col.attr("class");
    var colSearchPosition = classname.indexOf("col-");
    var colsize;

    do {
        if (colSearchPosition == -1) {
            break;
        }
        colsize = parseInt(classname.substr(colSearchPosition + 4, 2).trim());
        colSearchPosition = classname.indexOf("col-", colSearchPosition + 4);
        if (colSearchPosition >= classname.length) {
            break;
        }
    } while (isNaN(colsize) || colsize == '');

    if (isNaN(colsize) || colsize == '') {
        return 0;
    } else {
        return colsize
    }
}

//https://makandracards.com/makandra/13743-detect-effective-horizontal-pixel-width-on-a-mobile-device-with-javascript
function effectiveDeviceWidth() {
    var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
    // iOS returns available pixels, Android returns pixels / pixel ratio
    // http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
    if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
      deviceWidth = deviceWidth / window.devicePixelRatio;
    }
    return deviceWidth;
}