var nav = (function () {

    var latestWindowWidth;

    function adaptNav() {

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }

        // TODO: finish coding fixedNav option; dropdowns are not working
        if ($("nav").hasClass("fixedNav")) {
            $(".wrapper").css("padding-top", "59px");
        }

        // figure out how much space the elements in the nav take up
        var navElementsWidth = 0;
        $(".horizontalNav > ul > li").each(function (i) {
            var w = $(this).width();
            if(w < 54){w=54;}
            navElementsWidth += w;
        });

        // if the elements are wider than the window make a hamburger menu
        if (navElementsWidth + 30 > w) {
            // build the hamburger menu if it doesn't exist yet
            if (!$(".horizontalNav > ul > .hmenu").length) {
                /*
                hamburger hack, using 2x3 unicode horizontal bars to prevent blurryness
                or inconsisten thickness of lines (probably due to subpixel calculations)
                in scaled browser windows
                */
                $(".horizontalNav > ul").prepend("<li class='hmenu'><div class='hamburger'><div class='hbar1'>&#x2015;&#x2015;</div><div class='hbar2'>&#x2015;&#x2015;</div><div class='hbar3'>&#x2015;&#x2015;</div></div><ul></ul></li>");
                $(".horizontalNav > ul > li").not(".hmenu").not(".logo").not(".search")
                    .clone().appendTo(".hmenu > ul");
                $(".hmenu > ul > .right").removeClass("right");
            }
            $(".horizontalNav > ul > li").not(".hmenu").not(".logo").not(".search").hide();
            $(".hmenu").show();
        } else {
            // hide the hamburger menu
            $(".hmenu").hide();
            $(".horizontalNav > ul > li").not(".hmenu").show();
        }
    }

    function toggleHamburgerMenu(obj) {
        $(".hamburger").toggleClass("hchange");
        $(".hamburger").children().toggleClass("hchange");
        $(".dropdown > ul").hide();
        $(".hmenu > ul").not(obj.children("ul")).hide();
        obj.children("ul").toggle();
    }

    function closeAllMenus() {

        // hide nav dropdowns
        $(".dropdown > ul").hide();

        // hide hamburger menu
        $(".hamburger").removeClass("hchange");
        $(".hamburger").children().removeClass("hchange");
        $(".dropdown > ul").hide();
        $(".hmenu > ul").hide();
    }

    function toggleNavSearch() {

        /*
        if #navSearch form is not active, hide all elements except for search icon
        else rebuild the nav (closed search)
        */
        if ($("#navSearch").is(":hidden")) {
            $(".horizontalNav > ul > li").not(".search").hide();
        } else {
            adaptNav();
        }

        /*
        make the search icon to a back button and place it on the other side of the nav
        or build it back to normal
        */
        $(".search").toggleClass("right").toggleClass("left");
        $("#navSearch").toggle();
        if ($(".search > a > .navicon").attr("src") == "icons/baseline-search-24px.svg") {
            $(".search > a > .navicon").attr("src", "icons/baseline-chevron_left-24px.svg");
            $(".search > a > .navicon").attr("alt", "close search");
        } else {
            $(".search > a > .navicon").attr("src", "icons/baseline-search-24px.svg");
            $(".search > a > .navicon").attr("alt", "open search");
        }
    }

    function resize() {

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }

        /*
        Checking if window was resized vertically (e.g. on mobile when keyboard pops up),
        if resized vertically no reload is necessary. On vertical resize reload the popping
        up keyboard would close the search bar.
        */
        if (w != latestWindowWidth) {
            if ($("#navSearch").length && $("#navSearch").is(":visible")) {
                toggleNavSearch();
            }
            adaptNav();
            latestWindowWidth = w;
        } else {
            latestWindowWidth = w;
        }

//##############################################################################################
        // add dropdown class to uls with lis and add a little arrow
        $("ul > li").not(".hmenu").has("ul").addClass("dropdown");
        
        $(".dropdown > a").each(function(){
            if(!$(this).find("i.arrowdown").length){
                $(this).append(" <i aria-hidden='true' class='arrowdown'></i>");
            }
        });
        
        if($(".search").length) {
            if(!$(".horizontalNav").find("form#navSearch").length){
                $(".search").append("<a><img class='navicon' src='icons/baseline-search-24px.svg' alt='open search'></a>");
                $(".horizontalNav").append("<form id='navSearch' class='container' style='display:none'><button class='button' type='submit'><img class='navicon' src='icons/baseline-search-24px-w.svg' alt='search'></button><input type='text'></form>");
            }
        }
        

    }

    function init() {

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }

        // saving the last state of window width to compare later in resize()
        latestWindowWidth = w;

        // add dropdown class to uls with lis and add a little arrow
        $("ul > li").not(".hmenu").has("ul").addClass("dropdown");
        $(".dropdown > a").append(" <i aria-hidden='true' class='arrowdown'></i>");

        // add search form
        if ($(".search").length) {
            $(".search").append("<a><img class='navicon' src='icons/baseline-search-24px.svg' alt='open search'></a>");
            $(".horizontalNav").append("<form id='navSearch' class='container' style='display:none'><button class='button' type='submit'><img class='navicon' src='icons/baseline-search-24px-w.svg' alt='search'></button><input type='text'></form>");
        }

        // build and adapt nav
        adaptNav();



        // click handlers ############################################################

        $(document).on('click', function(event){
            // search icon to open search
            if($(event.target).is(".search > a") || $(event.target).parent().is(".search > a")){
                toggleNavSearch();
            // dropdown link    
            }else if($(event.target).is('nav > ul > .dropdown > a')){
                $(".dropdown > ul").not($(event.target).siblings("ul")).hide();
                $(".hmenu > ul").hide();
                $(event.target).siblings("ul").toggle();
            // hamburger icon
            }else if($(event.target).is('.hamburger') || $(event.target).is('.hbar1') || $(event.target).is('.hbar2') || $(event.target).is('.hbar3')){
                toggleHamburgerMenu($(event.target).closest(".hmenu"));
            // link in dropdown in hamburgermenu
            }else if($(event.target).is('.hmenu > ul > .dropdown > a')){
                $(".hmenu > ul > .dropdown > ul").not($(event.target).siblings("ul")).hide();
                $(event.target).siblings("ul").toggle();
            // click outside of the nav    
            }else{
                closeAllMenus();
            }
        });
    }

    return {
        init: init,
        resize: resize
    };

}());