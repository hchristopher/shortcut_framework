var various = (function () {

    function doDropdown() {
        $(".dropdownbtn").each(function () {
            var drop = $(this);
            // content to be dropped down, specified with its id in the data attribute of the
            // dropdown button
            var content = $("#" + drop.data("dropcontent"));
            var color = drop.data("color");

            /*
            add up and down chevron to indicate dropdown functionality if not has been added
            yet
            */
            if (!drop.children("img").first().length) {
                if (color == "g"){
                    drop.append("<img src='icons/baseline-expand_less-24px-g.svg' alt='close section'>");
                }else if (color == "w"){
                    drop.append("<img src='icons/baseline-expand_less-24px-w.svg' alt='close section'>");
                }else{
                    drop.append("<img src='icons/baseline-expand_less-24px.svg' alt='close section'>");
                }
                drop.children("img").first().css("vertical-align", "bottom");
                drop.children("img").first().css("padding-left", "5px");
            }

            //toggling dropdown, toggling up and down arrow and aria-labels
            var icon = drop.children("img").first();
            if (color == "g"){
                if (content.is(":visible")) {
                    icon.show();
                    icon.attr("alt", "close section");
                    icon.attr("src", "icons/baseline-expand_less-24px-g.svg");
                } else if (content.is(":hidden")) {
                    icon.show();
                    icon.attr("alt", "open section");
                    icon.attr("src", "icons/baseline-expand_more-24px-g.svg");
                }
            }else if (color == "w"){
                if (content.is(":visible")) {
                    icon.show();
                    icon.attr("alt", "close section");
                    icon.attr("src", "icons/baseline-expand_less-24px-w.svg");
                } else if (content.is(":hidden")) {
                    icon.show();
                    icon.attr("alt", "open section");
                    icon.attr("src", "icons/baseline-expand_more-24px-w.svg");
                }
            }else{
                if (content.is(":visible")) {
                    icon.show();
                    icon.attr("alt", "close section");
                    icon.attr("src", "icons/baseline-expand_less-24px.svg");
                } else if (content.is(":hidden")) {
                    icon.show();
                    icon.attr("alt", "open section");
                    icon.attr("src", "icons/baseline-expand_more-24px.svg");
                }
            }
        });
    }

    function scrollUpButton() {
        if ($(window).scrollTop() > 59) {
            $(".pageup").show();
        } else {
            $(".pageup").hide();
        }
    }

    function resize() {
        // set up dropdown
        $(".dropdownbtn").each(function () {
            var drop = $(this);
            if(!drop.hasClass("active")){
                var content = $("#" + drop.data("dropcontent"));
                content.hide();
            }
        });

        doDropdown();
        scrollUpButton();
    }

    function init() {

        // set up dropdown
        $(".dropdownbtn").each(function () {
            var drop = $(this);
            if(!drop.hasClass("active")){
                var content = $("#" + drop.data("dropcontent"));
                content.hide();
            }
        });

        doDropdown();
        scrollUpButton();

        // click handlers ##################################################

        // pageup button
        $(document).on('click', '.pageup', function () {
            //event.stopPropagation();
            $(document).scrollTop(0);
        });

        // dropdown buttons
        $(document).on('click', '.dropdownbtn', function () {
            //event.stopPropagation();
            $("#" + $(this).data("dropcontent")).toggle();
            doDropdown();
        });

        // scroll handler ##################################################
        // show pageup button
        $(document).on('scroll', function () {
            if ($(document).scrollTop() > 59) {
                $(".pageup").show();
            } else {
                $(".pageup").hide();
            }
        });
    }

    return {
        init: init,
        resize: resize
    };

}());