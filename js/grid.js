var grid = (function () {

    var desktopState;
    
    var desktopViewCreated;

    function createMobileView(wwidth) {

        $(".row").each(function (i) {
            var row = $(this);

            // go to next row if user wishes not to change layout
            if (row.hasClass("no-change")) {
                return true;
            }

            // array for rows that have space for additional cols
            var waitingCol = [];

            //do prioritized cols first to simplify sorting process
            // prio cols ##################################################
            row.children("[class*='prio']").each(function (j) {
                var col = $(this);

                // mobile prio ############################################################
                if (wwidth <= 576) {
                    col.addClass("col-m-12");
                    // if prio elements already placed add current one after them
                    if (waitingCol.length) {
                        waitingCol[waitingCol.length - 1].element.after(col);
                    } else {
                        col.prependTo(row);
                        waitingCol.push({ "position": j, "space": 0, "element": col });
                    }
                    // tablet prio ############################################################    
                } else if (wwidth <= 768 && wwidth > 576) {
                    if (col.is(".col-7,.col-8,.col-9,.col-10,.col-11,.col-12")) {
                        col.addClass("col-t-12");
                        if (waitingCol.length) {
                            waitingCol[waitingCol.length - 1].element.after(col);
                        } else {
                            col.prependTo(row);
                        }
                    } else {
                        var elementPlaced = false;
                        col.addClass("col-t-6");
                        if (waitingCol.length) {
                            for (var q = 0; q < waitingCol.length; q++) {
                                // if there is a row that isn't filled add it
                                if (waitingCol[q].space >= 6) {
                                    waitingCol[q].element.after(col);
                                    waitingCol[q].space = 0;
                                    col.next().addClass("temprow");
                                    elementPlaced = true;
                                    break;
                                }
                                // if not, it will be an own row and added to the open rows
                                // this part is in the for loop because the element has to
                                // be placed right after the last prio element
                                if (!elementPlaced && q == waitingCol.length - 1) {
                                    waitingCol[q].element.after(col);
                                    waitingCol.push({ "position": j, "space": 6, "element": col });
                                }
                            }
                            // if it's the first prio element
                        } else {
                            col.prependTo(row);
                            waitingCol.push({ "position": j, "space": 6, "element": col });
                        }
                    }
                }
            });

            // non-prio cols ############################################################
            row.children("[class*='col-']").each(function (j) {
                var col = $(this);
                if (col.hasClass("prio")) {
                    return true;
                }

                // mobile ############################################################
                if (wwidth <= 576) {
                    // small elements go half width    
                    if (col.is(".col-3, .col-2, .col-1")) {
                        col.addClass("col-m-6");
                        // functionality to fill rows, half element is saved in waitingCol
                        // when another half element shows up it is placed after waitingCol
                        var elementPlaced = false;
                        for (var q = 0; q < waitingCol.length; q++) {
                            if (waitingCol[q].space >= 6) {
                                waitingCol[q].element.after(col);
                                waitingCol[q].space = 0;
                                col.next().addClass("temprow");
                                elementPlaced = true;
                                break;
                            }
                        }
                        if (!elementPlaced) {
                            waitingCol.push({ "position": j, "space": 6, "element": col });
                        }
                    } else {
                        col.addClass("col-m-12");
                    }
                }
                // tablet ############################################################
                if (wwidth <= 768 && wwidth > 576) {
                    // get the number in the col classname
                    var colsize = getColSize(col);

                    if (colsize == 0) {
                        col.hide();
                        return true;
                    }
                    // --- small elements go quarter width   
                    if (colsize <= 3) {
                        col.addClass("col-t-3");
                        colsize = 3;
                        // functionality to fill rows, half element is saved in waitingCol
                        // when another half element shows up it is placed after waitingCol
                        var elementPlaced = false;
                        for (var q = 0; q < waitingCol.length; q++) {
                            if (waitingCol[q].space >= colsize) {
                                waitingCol[q].element.after(col);
                                // change data to the object that got moved
                                waitingCol[q].space -= colsize;
                                elementPlaced = true;
                                waitingCol[q].element = col;
                                if (waitingCol[q].space == 0) {
                                    col.next().addClass("temprow");
                                }
                                break;
                            }
                        }
                        if (!elementPlaced) {
                            waitingCol.push({ "position": j, "space": 12 - colsize, "element": col });
                        }
                        // --- large elements go full width
                    } else if (colsize >= 10) {
                        col.addClass("col-t-12");
                        // --- everything in between stays the same
                    } else {
                        col.addClass("col-t-" + colsize);
                        var elementPlaced = false;
                        for (var q = 0; q < waitingCol.length; q++) {
                            if (waitingCol[q].space >= colsize) {
                                waitingCol[q].element.after(col);
                                // change data to the object that got moved
                                waitingCol[q].space -= colsize;
                                elementPlaced = true;
                                waitingCol[q].element = col;
                                if (waitingCol[q].space == 0) {
                                    col.next().addClass("temprow");
                                }
                                break;
                            }
                        }
                        if (!elementPlaced) {
                            waitingCol.push({ "position": j, "space": 12 - colsize, "element": col });
                        }
                    }
                }
            });
        });
    }

    function createDesktopView(){
        $(".row").each(function (i) {
            var row = $(this);
            
            // calculate how much the col elements sum up to
            var sumColSize = 0;
            row.children("[class*='col-']").each(function (j) {
                var col = $(this);
                sumColSize += getColSize(col);
            });

            // how many number of rows that makes
            var numberOfRows = Math.trunc(sumColSize / 12);

            // try to keep the ratio, while sizing the cols down to fit into a single row
            // no float sizes, always round up
            sumColSize = 0;
            row.children("[class*='col-']").each(function (j) {
                var col = $(this);
                var colsize = getColSize(col);
                var newcolsize = Math.ceil(colsize/numberOfRows);
                sumColSize += newcolsize;
                col.addClass("col-d-" + newcolsize);
            });

            // if the sum of colwidths still over 12, take away from the largest elements
            for(var i = 0; i < sumColSize-12; i++){
                var largestColSize = 0;
                var largestColIndex = 0;
                row.children("[class*='col-']").each(function (j) {
                    var col= $(this);
                    if (getColSize(col) > largestColSize){
                        largestColIndex = j;
                    }
                });
                var resizeElement = row.children(":nth-child(" + largestColIndex + ")");
                var classString = resizeElement.attr("class");
                var colStringPos = parseInt(classString.search("col-d-")) + 6;
                var dcolsize = parseInt(classString.substr(colStringPos,2).trim());
                dcolsize -= 1;
                classString = classString.substr(0,colStringPos) + dcolsize + " " + classString.substr(colStringPos+2).trim();
                classString = classString.trim();
                resizeElement.attr("class", classString);
            }
        });
        desktopViewCreated = true;
    }

    function doTemprows(){
        $(".row").each(function (i) {
            var row = $(this);
            row.children("[class*='col-']").not(":first").each(function (j) {
                var col = $(this);
                // make a clean row when the previous element is not in the same row
                if (col.position().top != col.prev().position().top) {
                    col.addClass("temprow");
                }
            });
        });
    }

    function resize() {

        /*
        temprows are used to keep objects aligned in clean rows, even if they
        are put in a new order, however on resize the gridbuild should start with
        a clean state, therefor all temprows must be removed
        */
        $(".temprow").removeClass("temprow");

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }

        if ($("body").hasClass("desktop")) {
            if (w > 768) {
                $(".wrapper").html(desktopState);
                doTemprows();
            } else if (!$("body").hasClass("no-change")) {
                createMobileView(w);
            }
        }else if($("body").hasClass("mobile")){
            if (w > 768 && !desktopViewCreated) {
                createDesktopView();
            }else{
                doTemprows();
            }
        }
    }

    function init() {

        // only let createDesktopView run once, because it only sets col-d-* classes
        desktopViewCreated = false;

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }
        
        if ($("body").hasClass("desktop")) {
            /*
            because a lot of elements will be rearranged it is important to save
            the order intended by the user to be able to safely return to the indended
            desktop state when resizing back and forth
            */
            desktopState = $(".wrapper").html();
            if (w <= 768) {
                createMobileView(w);
            }else{
                doTemprows();
            }
        }else if($("body").hasClass("mobile")){
            if (w > 768 && !desktopViewCreated) {
                createDesktopView();
            }else{
                doTemprows();
            }
        }

    }

    return {
        init: init,
        resize: resize
    };

}());