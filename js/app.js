// the order of execution is important to some functions
$(window).on('load', function () {
    $.when(grid.init())
        .done(nav.init())
        .done(image.init())
        .done(scarousel.init())
        .done(slist.init())
        .done(divcompact.init())
        .done(footer.init())
        .done(tabs.init())
        .done(accordion.init())
        .done(paragraph.init())
        .done(various.init())
        .done(footer.sticky())
        .done($(".loader").hide());
});

//only do resize operation when done resizing to save resources and prevent bugs
var resizeTimeout;
$(window).resize(function () {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(doneResizing, 500);
});

function doneResizing() {
    $.when(grid.resize())
        .done(nav.resize())
        .done(image.resize())
        .done(scarousel.resize())
        .done(slist.resize())
        .done(divcompact.resize())
        .done(footer.init())
        .done(tabs.resize())
        .done(accordion.resize())
        .done(paragraph.resize())
        .done(various.resize())
        .done(footer.sticky());
}