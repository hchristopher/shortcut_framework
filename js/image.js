var image = (function () {

    // 0-12 in percentage values
    var coords = [0, 0.0833, 0.1666, 0.25, 0.3333, 0.4166, 0.5, 0.5833, 0.6666, 0.75, 0.8333, 0.9166, 1]

    function focusImage() {

        /*
        the image will be set as background of the container and
        moved around according to the current display situation
        */

        // get image information
        var i = $(this);
        i.hide();
        var iwidth = i.width();
        var iheight = i.height();
        var iurl = i.attr("src");

        // get container information
        var c = i.parent();
        var cwidth = c.width();

        // move aria from image to container
        c.attr("aria-label", i.attr("alt"));

        // define the focus part
        // get values from html and convert to int
        var coordvalues = [];
        if (!i.data("img-focus")) {
            // if no focus is set, focus whole image
            coordvalues = [0, 0, 12, 12];
        }else{
            var preValues = i.data("img-focus").split(",");
            for(var j = 0; j < preValues.length; j++){
                coordvalues[j] = parseInt(preValues[j]);
            }
        }

        // convert values to percentage values with coordvalues
        var xstart = iwidth * coords[coordvalues[0]];
        var ystart = iheight * coords[coordvalues[1]];
        var xend = iwidth * coords[coordvalues[2]];
        var yend = iheight * coords[coordvalues[3]];

        var partwidth = xend - xstart;
        var partheight = yend - ystart;

        var xratio = cwidth / partwidth;

        // calculate new image size so focus part fits container
        if (xratio < 1) {
            iwidth = iwidth * xratio;
            iheight = iheight * xratio;
            partwidth = partwidth * xratio;
            partheight = partheight * xratio;
            xstart = iwidth * coords[coordvalues[0]];
            ystart = iheight * coords[coordvalues[1]];
        }
        c.height(partheight);

        // placement of focus part
        var placement;
        if (!i.data("img-placement")) {
            placement = ["center", "center"];
        } else {
            placement = i.data("img-placement").split(",");
        }

        // if container is wider than the image part
        // horizontal placement
        if (xratio > 1) {
            var xchange = cwidth - partwidth;
            if (placement[0] == "left") {
                //nothing to do, however center is standard
            } else if (placement[0] == "right") {
                xstart -= xchange;
            } else { // center
                xchange = xchange / 2;
                xstart -= xchange;
            }
        }

        // get the user specified image height
        // vertical placement
        var imgheight = parseInt(i.data("img-height"));
        if (typeof imgheight === 'number' && (imgheight % 1) === 0) {
            var ychange = imgheight - partheight;
            if (placement[1] == "top") {
                c.height(imgheight);
            } else if (placement[1] == "bottom") {
                ystart -= ychange;
                c.height(imgheight);
            } else { //center
                ychange = ychange / 2;
                ystart -= ychange;
                c.height(imgheight);
            }
        }

        xstart = xstart * -1;
        ystart = ystart * -1;

        // positioning with calculated values
        c.css("background", "url('" + iurl + "')");
        c.css("background-repeat", "no-repeat");
        c.css("background-size", iwidth + "px " + iheight + "px");
        c.css("background-position", xstart.toString() + "px " + ystart.toString() + "px");

    }

    function resize() {
        $(".autofocus").each(focusImage);
    }

    function init() {
        $(".autofocus").each(focusImage);
    }

    return {
        init: init,
        resize: resize
    };

}());