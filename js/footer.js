var footer = (function () {

    function sizeFooter() {
        /*
        creating category dropdowns (e.g. a sitetree in the footer), dropdown only
        when tablet or mobile size
        */

       var w = window.innerWidth
       || document.documentElement.clientWidth
       || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }

        if (w <= 768) {
            $(".sitetree").each(function () {
                var idpart = $(this).attr("class").replace(/ /g, '');
                var diffcolor;
                if($(this).hasClass("lowerFooter")){ 
                    diffcolor = "g";
                }
                $(this).find(".footerListHeading").each(function (index) {
                    var head = $(this);
                    head.addClass("dropdownbtn");
                    if(diffcolor){head.data("color", diffcolor)}
                    head.parent().addClass("col-m-12");
                    head.parent().addClass("col-t-6");
                    if (index % 2 == 0) {
                        head.parent().addClass("temprow");
                    }
                    head.css("border-bottom", "1px solid");
                    head.attr("data-dropcontent", idpart + "FooterList" + index);
                    var cont = head.next("ul");
                    cont.attr("id", idpart + "FooterList" + index);
                });
            });
        } else {
            $(".sitetree").each(function () {
                $(this).find(".footerListHeading").each(function (index) {
                    var head = $(this);
                    head.removeClass("dropdownbtn");
                    head.parent().removeClass("col-m-12");
                    head.parent().removeClass("col-t-12");
                    head.css("border-bottom", "none");
                    head.next("ul").show();
                    head.children("img").hide();
                });
            });
        }
    }

    function sticky() {
        /*
        sticky footer functionality, make sure that footer is alway on the bottom of
        the page, even if the page is not filled completely
        */
       var h = $("footer").height();
       $(".wrapper").css("min-height", "calc(100% - " + $('.header').height() + "px)");
       $(".wrapper").css("margin-bottom", "-" + h + "px");
       $(".footer-push").css("height",h + "px");
    }

    function resize() {
        sizeFooter();
    }

    function init() {
        sizeFooter();
    }

    return {
        init: init,
        resize: resize,
        sticky: sticky
    };

}());