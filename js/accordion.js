var accordion = (function () {

    function resize() {

        // bring each accordion back to its initial state
        $(".accordion").each(function () {
            var a = $(this);
            if (!a.find("img").length) {
                a.find(".abutton").append("<img class='accicon right' src='icons/baseline-expand_more-24px.svg' alt='open section'>");
            }
            a.find(".active").children(".accicon").attr("src", "icons/baseline-expand_less-24px.svg");
            a.find(".active").children(".accicon").attr("alt", "close section");
            a.find(".acontent").hide();
            a.find(".active").next(".acontent").show();
        });
    }

    function init() {

        // add chevron and aria to button
        $(".accordion").each(function () {
            var a = $(this);
            a.find(".abutton").append("<img class='accicon right' src='icons/baseline-expand_more-24px.svg' alt='open section'>");
            a.find(".active").children(".accicon").attr("src", "icons/baseline-expand_less-24px.svg");
            a.find(".active").children(".accicon").attr("alt", "close section");
            a.find(".acontent").hide();
            a.find(".active").next(".acontent").show();
        });

        // accordion button click handler
        $(document).on('click', '.accordion > .abutton', function () {
            //event.stopPropagation();
            var ab = $(this);

            if (ab.next(".acontent").is(":hidden")) {
                ab.siblings(".acontent").hide();
                ab.parent().find(".accicon").attr("src", "icons/baseline-expand_more-24px.svg");
                ab.parent().find(".accicon").attr("alt", "open section");
                ab.siblings(".active").removeClass("active");
                ab.next(".acontent").show();
                ab.children(".accicon").attr("src", "icons/baseline-expand_less-24px.svg");
                ab.children(".accicon").attr("alt", "close section");
                ab.addClass("active");
            } else {
                ab.siblings(".acontent").hide();
                ab.children(".accicon").attr("src", "icons/baseline-expand_more-24px.svg");
                ab.children(".accicon").attr("alt", "open section");
                ab.removeClass("active");
            }
        });

    }

    return {
        init: init,
        resize: resize
    };

}());