var tabs = (function () {

    function doMobileTabs(tabs) {
        // create mobile tabs without triangle
        tabs.find(".triangle").hide();
        tabs.addClass("tabs-m");
        tabs.children().outerWidth((tabs.width() - (4 * tabs.children().length)) / tabs.children().length);
    }

    function doTabs() {
        $(".tabs").each(function () {
            var tabs = $(this);

            // rebuild tabs in case function is called from a resize from mobile
            tabs.removeClass("tabs-m");
            tabs.find(".triangle").show();
            tabs.children().css("width", "unset");
            tabs.children().css("padding", "14px 16px");

            // open the active tab
            var tabdata = $(this).children(".active").data("tab");
            $("#" + tabdata).show();

            // make all tabs the length of the longest tab
            var longestWidth = 0;
            tabs.children().each(function (i) {
                var tab = $(this);
                if (!tab.find(".triangle").length) {
                    tab.append("<div class='triangle'></div>");
                }
                if (tab.width() > longestWidth) {
                    longestWidth = tab.width();
                } else {
                    tabs.children().width(longestWidth);
                }
            });

            // calculate the width of all tabs together and if its higher than the container
            // width, create a different kind of mobile tabs
            var elementWidth = 0;
            tabs.children().each(function (i) {
                elementWidth += $(this).width() + 48; /* padding and triangle */
            });
            if (elementWidth > tabs.parent().width()) {
                doMobileTabs(tabs);
            }
        });
    }

    function resize() {
        doTabs();
    }

    function init() {

        doTabs();

        // click handlers for desktop and mobile version tabs

        $(document).on('click', '.tabs > div', function () {
            //event.stopPropagation();
            var tab = $(this);
            tab.siblings().each(function () {
                var ts = $(this);
                $("#" + ts.data("tab")).hide();
                ts.removeClass("active");
            });
            $("#" + tab.data("tab")).show();
            tab.addClass("active");
        });

        $(document).on('click', '.tabs-m > div', function () {
            //event.stopPropagation();
            var tab = $(this);
            tab.siblings().each(function () {
                var ts = $(this);
                $("#" + ts.data("tab")).hide();
                ts.removeClass("active");
            });
            $("#" + tab.data("tab")).show();
            tab.addClass("active");
        });
    }

    return {
        init: init,
        resize: resize
    };

}());