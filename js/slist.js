var slist = (function () {

    /*
    using slick as specified in slick documentation
    dnum is the number of items displayed at once in one row defined by user
    snum, tnum, mnum are calculated to show less items in a row for different
    display sizes
    */

    function buildList(list) {
        var dnum;
        var snum;
        var tnum;
        var mnum;
        if ($("body").hasClass("desktop")) {
            dnum = parseInt(list.data("slidesdisplayed"));
            snum = Math.trunc(dnum * 0.75);
            tnum = Math.trunc(snum * 0.75);
            mnum = Math.trunc(tnum * 0.75);
        }else if ($("body").hasClass("mobile")){
            mnum = parseInt(list.data("slidesdisplayed"));
            tnum = Math.trunc(dnum * 1.25);
            snum = Math.trunc(snum * 1.25);
            dnum = Math.trunc(tnum * 1.25);
        }
        list.slick({
            dots: true,
            infinite: true,
            slidesToShow: dnum,
            slidesToScroll: dnum,
            prevArrow: "<a class='sprev' aria-label='previous item'><i class='material-icons'>chevron_left</i></a>",
            nextArrow: "<a class='snext' aria-label='next item'><i class='material-icons'>chevron_right</i></a>",
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: snum,
                        slidesToScroll: snum,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: tnum,
                        slidesToScroll: tnum,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: mnum,
                        slidesToScroll: mnum,
                    }
                }
            ]
        });
    }

    function resize() {
        $(".slist").each(function () {
            if ($(this).hasClass("slick-initialized")) {
                $(this).slick('unslick');
            }
            buildList($(this));
        });
    }

    function init() {

        $(".slist").each(function () {
            if ($(this).hasClass("slick-initialized")) {
                $(this).slick('unslick');
            }
            buildList($(this));
        });

    }

    return {
        init: init,
        resize: resize
    };

}());