var divcompact = (function () {

    function buildDivCompact(c) {

            var rows = c.data("rows");
            var rowcount = 1;

            /*
            go through the elements, count how many rows by checking if the previous
            element is offset, if the number of rows is higher than the data attribute
            hide the current and previous element (previous one for space for the button)
            and all after
            */
            c.children("div").not(":first").each(function () {
                var col = $(this);
                    if (col.position().top != col.prev().position().top) {
                        rowcount++;
                    }
                    if (rowcount > rows) {
                        col.prev().hide();
                        col.hide();
                        col.nextAll().not(".reveal").hide();
                        return false;
                    }
                
            });

            // content that gets revealed
            var revealContent = c.children(".reveal");

            // build for link design or button design - centering inside the space
            if (revealContent.find("a").length) {
                var margintop = (c.children(".reveal").prevAll(":visible:first").innerHeight() / 2) - (revealContent.find("a").innerHeight() / 2);
                var marginleft = (revealContent.width() / 2) - (revealContent.find("a").width() / 2) - 10;
                revealContent.find("a").css("margin-top", margintop + "px");
                revealContent.find("a").css("margin-left", marginleft + "px");
            } else {
                revealContent.find(".close").hide();
                revealContent.find(".open").show();
                var margintop = (c.children(".reveal").prevAll(":visible:first").innerHeight() / 2) - (revealContent.find(".button").innerHeight() / 2);
                var marginleft = (revealContent.width() / 2) - (revealContent.find(".button").width() / 2) - 10;
                revealContent.find(".button").css("margin-top", margintop + "px");
                revealContent.find(".button").css("margin-left", marginleft + "px");
            }
    }

    function toggleCompact(d) {

        // if all content is shown hide stuff, else show more stuff
        if (d.parent().hasClass("revealhide")) {
            d.parent().removeClass("revealhide");
            buildDivCompact(d.closest(".divcompact"));
        } else {
            d.closest(".divcompact").children().show();
            d.parent().children(".open").hide();
            d.parent().children(".close").show();
            d.parent().addClass("revealhide");
        }
    }

    function resize() {

        // make sure all children are visible before starting
        $(".divcompact").children().show();
        // remove temprow at reveal, it misplaces the reveal button
        $(".divcompact").find(".reveal").removeClass("temprow");

        $(".divcompact").each(function(){
            buildDivCompact($(this));
        });

    }

    function init() {

        // remove temprow at reveal, it misplaces the reveal button
        $(".divcompact").find(".reveal").removeClass("temprow");

        $(".divcompact").each(function(){
            buildDivCompact($(this));
        });

        // click handler for button to reveal more or less content
        $(document).on('click', '.reveal > div', function (event) {
            event.stopPropagation();
            toggleCompact($(this));
        });

    }

    return {
        init: init,
        resize: resize
    };

}());