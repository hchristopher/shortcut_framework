var paragraph = (function () {

    function doParagraph() {

        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            w = window.orientation == 0 ? window.screen.width : window.screen.height;
            w = w / window.devicePixelRatio;
        }
        
        // p with multi column layout
        $(".multi-p").each(function () {

            var m = $(this);

            if (w > 768) {
                m.find(".open").hide();
                m.find(".close").hide();
                // in case the function got called by resizing from mobile
                m.removeClass("single-p");
                // user defined number of columns
                var columns = parseInt(m.data("columns"));
                /*
                each column contains the full p, however the column doesn't display overflow
                so p is "scrolled" in place for each column
                */
                if (m.children("p").length < columns) {

                    var startp = m.find("p").first();
                    var viewheight = m.data("height"); /* multi of 27px lineheight*/

                    for (var i = 1; i < columns; i++) {
                        startp.clone().appendTo(m);
                    }
                    m.css("height", viewheight + "px");
                    $(this).children("p").each(function (i) {

                        var p = $(this);

                        p.css("width", (100 / columns) + "%");
                        p.css("float", "left");
                        // offset the text by the height of the previous columns
                        p.css("margin-top", "-" + (viewheight * i) + "px");
                    });
                }
                // --- single column layout if screen width is tablet or mobile size
            } else {
                // remove multi-p changes
                if (m.children("p").length > 1) {
                    var startp = m.find("p").first();
                    m.children("p").not(startp).remove();

                    var viewheight = m.data("height");
                    m.css("height", viewheight + "px");

                    var p = m.children("p");

                    p.css("width", "auto");
                    p.css("float", "none");
                    p.css("margin-top", "0px");
                }
                m.addClass("single-p");
            }
        });

        // p with single column layout
        $(".single-p").each(function () {
            var p = $(this);

            // set height
            if (p.data("height")) {
                p.children("p").css("height", p.data("height") + "px");
            }

            // create buttons
            var open = p.find(".open");
            var close = p.find(".close");
            if (!open.find(".button").length) {
                var text = open.html();
                open.html("<div class='button'>" + text + "</div>");
                open.addClass("textfade");
            }
            open.show()
            if (!close.find(".button").length) {
                var text = close.html();
                close.html("<div class='button'>" + text + "</div>");
                close.addClass("textnofade");
            }
            close.hide();
        });
    }

    function resize() {

        doParagraph();

    }

    function init() {

        doParagraph();


        // click handlers for open and close button

        $(document).on('click', '.single-p .open', function () {
            //event.stopPropagation();
            var open = $(this);
            open.parent().css("height", "auto");
            open.siblings("p").css("height", "auto");
            open.hide();
            open.siblings(".close").show();
        });

        $(document).on('click', '.single-p .close', function () {
            //event.stopPropagation();
            var close = $(this);
            var oldheight = close.parent().data("height");
            close.parent().css("height", oldheight + "px");
            close.siblings("p").css("height", oldheight + "px");
            close.hide();
            close.siblings(".open").show();
        });

    }

    return {
        init: init,
        resize: resize
    };

}());