var scarousel = (function () {

    // using slick functions as specified in slick documentation
    // adding own prev and next arrows with aria labels

    function resize() {
        if ($(".scarousel").hasClass("slick-initialized")) {
            $(".scarousel").slick('unslick');
        }
        $(".scarousel").slick({
            prevArrow: "<a class='sprev' aria-label='previous item'><i class='material-icons'>chevron_left</i></a>",
            nextArrow: "<a class='snext' aria-label='next item'><i class='material-icons'>chevron_right</i></a>",
            dots: true
        });
    }

    function init() {
        if ($(".scarousel").hasClass("slick-initialized")) {
            $(".scarousel").slick('unslick');
        }
        $(".scarousel").slick({
            prevArrow: "<a class='sprev' aria-label='previous item'><i class='material-icons'>chevron_left</i></a>",
            nextArrow: "<a class='snext' aria-label='next item'><i class='material-icons'>chevron_right</i></a>",
            dots: true
        });
    }

    return {
        init: init,
        resize: resize
    };

}());