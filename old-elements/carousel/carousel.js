var carousel = (function(){

    function createSlider(c){
        
            var slide = c.find(".slide").not(":hidden");
            /*carousel.find(".prev").prependTo(slide);
            carousel.find(".next").appendTo(slide);*/
            c.find(".prev").css("line-height",slide.height() + "px");
            c.find(".next").css("line-height",slide.height() + "px");
        
    }

    function nextSlide(c){
        setTimeout(2000);
        var current = c.children(".slide").not(":hidden");
        current.hide();
        if(current.next(".slide").length){
            current.next(".slide").show();
            createSlider(c);
        }else{
            c.children(".slide").first().show();
            createSlider(c);
        }
        setTimeout(function(){
            nextSlide(c);
        },5000);
    }
    
    $(window).resize(function(){
        $(".carousel").children(".slide").not(":first").hide();
        $(".carousel").each(function(i){
            createSlider($(this));
        });
    });
    
    function init(){
        $(".carousel").children(".slide").not(":first").hide();
        $(".carousel").each(function(i){
            createSlider($(this));
        });

        if($(".carousel").hasClass("autonext")){
            setTimeout(function(){
                nextSlide($('.carousel'));
            },5000);
        }
        
        
        $(".prev").click(function(event){
            event.stopPropagation();
            var current = $(this).siblings(".slide").not(":hidden");
            current.hide();  
            if(current.prev(".slide").length){
                current.prev(".slide").show();
                createSlider(current.closest(".carousel"));
            }else{
                current.closest(".carousel").children(".slide").last().show();
                createSlider(current.closest(".carousel"));
            }
        });

        $(".next").click(function(event){
            event.stopPropagation();
            var current = $(this).siblings(".slide").not(":hidden");
            current.hide();
            if(current.next(".slide").length){
                current.next(".slide").show();
                createSlider(current.closest(".carousel"));
            }else{
                current.closest(".carousel").children(".slide").first().show();
                createSlider(current.closest(".carousel"));
            }
        });
    }

    return{
        init:init
    };

}());